import React, { useState } from 'react';
import './TweetBox.css';
import { Avatar, Button } from '@material-ui/core';
import db from './firebase'

function TweetBox() {
  const [tweetMessage, setTweetMessage] = useState('');
  const [tweetImage, setTweetImage] = useState('');

  const sendTweet = e => {
    e.preventDefault();

    db.collection('posts').add({
      displayName: 'Rizal Fauzi',
      username: 'rizalfauziwahyu',
      verified: true,
      text: tweetMessage,
      image: tweetImage,
      avatar: 'https://robohash.org/fauzi?size=200x200&bgset=bg1'
    });

    setTweetImage('');
    setTweetMessage('');
  }

  return (
    <div className='tweetBox'>
      <form>
        <div className='tweetBox__input'>
          <Avatar src='https://robohash.org/test?size=200x200'></Avatar>
          <input
            onChange={(e) => setTweetMessage(e.target.value)}
            value={tweetMessage}
            placeholder="What's happening?"
            type='text'
          />
        </div>
        <input
          onChange={(e) => setTweetImage(e.target.value)}
          value={tweetImage}
          className='tweetBox__imageInput'
          placeholder='Optional: Enter image URL'
          type='text'
        />
      </form>
      <Button onClick={sendTweet} type='submit' className='tweetBox__tweetButton'>
        Tweet
      </Button>
    </div>
  );
}

export default TweetBox;
