import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyCQPi0q-a20msd2YCGxGVLTuQXisW447kw',
  authDomain: 'twitter-clone-881c5.firebaseapp.com',
  databaseURL: 'https://twitter-clone-881c5.firebaseio.com',
  projectId: 'twitter-clone-881c5',
  storageBucket: 'twitter-clone-881c5.appspot.com',
  messagingSenderId: '24379302380',
  appId: '1:24379302380:web:fdb6224984889c044f60c9',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();

export default db;